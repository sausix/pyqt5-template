# PyQt5 Template Class

Practical and simple wrapper classes for using Qt5 with Python.


Provides classes and a bare template for windows and dialogs used with Qt5 and Python.
Uses external ui files and adds support for saving settings including window position and size and other control's data.
