from datetime import datetime, date
from PyQt5.QtCore import QSettings, QPoint, QSize, Qt, QObject, QDate, QDateTime, pyqtBoundSignal
from PyQt5.QtWidgets import QMainWindow, QDialog, QDateTimeEdit, QCheckBox, QLineEdit, QComboBox, QDockWidget, QWidget
from PyQt5.uic import loadUi


# QDateTimeEdit
def dte(widget: QDateTimeEdit):
    def set_value(value):
        if value is None:
            widget.setDate(datetime.today().date())
        else:
            if type(value) is str:
                widget.setDate(datetime.strptime(value, '%Y-%m-%d').date())
            if type(value) is date or type(value) is QDate:
                widget.setDate(value)
            if type(value) is datetime or type(value) is QDateTime:
                widget.setDateTime(value)

    def get_value() -> QDate:
        return widget.date()

    def get_value_str():
        return get_value().toPyDate().strftime('%Y-%m-%d')

    widget.Set_Value = set_value
    widget.Get_Value = get_value
    widget.Get_Value_Str = get_value_str

    return widget.dateTimeChanged  # TODO args?


# QCheckBox
def chb(widget: QCheckBox):
    def set_value(value):
        if value is None:
            widget.setChecked(False)
        else:
            if type(value) is bool:
                widget.setChecked(value)
            if type(value) is int:
                widget.setChecked(bool(value))
            if type(value) is str:
                widget.setChecked(value.lower() in ['true', '1'])

    def get_value() -> bool:
        return widget.isChecked()

    def get_value_str():
        return str(get_value())

    widget.Set_Value = set_value
    widget.Get_Value = get_value
    widget.Get_Value_Str = get_value_str

    return widget.stateChanged  # TODO args?


# QComboBox
def cb(widget: QComboBox):
    def set_value(value):
        index = widget.findData(value)
        widget.setCurrentIndex(index)  # Not found: -1

    def get_value() -> bool:
        return widget.currentData()

    def get_value_str():
        return get_value()

    widget.Set_Value = set_value
    widget.Get_Value = get_value
    widget.Get_Value_Str = get_value_str

    return widget.currentIndexChanged  # int index


# QLineEdit
def qle(widget: QLineEdit):
    def set_value(value):
        if value is None:
            widget.setText('')
        else:
            if type(value) is str:
                widget.setText(value)
            if type(value) is int:
                widget.setText(str(value))
            else:
                widget.setText(str(value))

    def get_value() -> str:
        return widget.text()

    def get_value_str() -> str:
        return get_value()

    widget.Set_Value = set_value
    widget.Get_Value = get_value
    widget.Get_Value_Str = get_value_str

    return widget.returnPressed  # TODO args?


# QDockWidget
def dw(widget: QDockWidget):
    # Set default dock position
    widget.dockloc = 1

    def set_value(value: str):
        # parse string
        elems = value.split(',')
        if len(elems) == 6:
            window: QMainWindow = widget.parent()

            # No direct method available
            window.removeDockWidget(widget)
            window.addDockWidget(int(elems[1]), widget)

            widget.setFloating(elems[0] == '1')

            # For initial size when docked
            widget.widget().sizeHint = lambda: QSize(int(elems[4]), int(elems[5]))

            # For floating state
            widget.setGeometry(int(elems[2]), int(elems[3]), int(elems[4]), int(elems[5]))

            # Show it
            widget.show()

    def get_value() -> str:
        return str(int(widget.isFloating())) + ',' + str(widget.dockloc) + ',' + str(widget.pos().x()) + ',' + str(widget.pos().y()) + ',' + str(widget.width()) + ',' + str(widget.height())

    def get_value_str():
        return get_value()

    widget.Set_Value = set_value
    widget.Get_Value = get_value
    widget.Get_Value_Str = get_value_str

    def changed_area(w, area):
        # Always track current position
        w.dockloc = area

    widget.dockLocationChanged.connect(lambda newarea: changed_area(widget, newarea))
    return None


class SaveWindow(QObject):
    def __init__(self):
        QObject.__init__(self)
        self.__positioned = False
        self.__widgets_data = list()
        self.__widgets_size = list()
        # Supported Widgets ###################
        self.__handler = {
            QDateTimeEdit: dte,
            QCheckBox: chb,
            QLineEdit: qle,
            QComboBox: cb,
            QDockWidget: dw,
        }
        #######################################

    def Settings(self) -> QSettings:
        s = QSettings()
        s.beginGroup('windows/' + self.objectName())
        return s

    def SettingsSave(self):
        window: QWidget = self
        s = self.Settings()
        # s.setValue("size", self.size())
        # s.setValue("pos", self.pos())
        s.setValue("geometry", window.saveGeometry())
        s.endGroup()
        s.sync()

    def SettingsRestore(self):
        s = self.Settings()

        window: QWidget = self
        
        geo = s.value("geometry", None)
        if geo is None:
            self.resize(s.value("size", QSize(270, 225)))
            self.move(s.value("pos", QPoint(50, 50)))
        else:
            print(geo)
            window.restoreGeometry(geo)

        s.endGroup()

    def RegisterWidget(self, widget, default=None, changefunc=None, individualsettings=None):
        widget.DefaultValue = default
        widget.Individualsettings = individualsettings
        widget.__func = changefunc
        widget.__set_data = True
        self.__widgets_data.append(widget)

    def RegisterSize(self, widget):
        widget.__set_size = True
        self.__widgets_size.append(widget)

    def __widgets_setup(self):
        s = self.Settings()

        for widget in self.__widgets_data:  # type: QWidget
            wtype = type(widget)
            if wtype in self.__handler:
                signal = self.__handler[wtype](widget)

                # Set saved value
                try:
                    widget.Set_Value(s.value(widget.objectName(), widget.DefaultValue))
                except:
                    pass

                # Optionally connect event handler
                if type(signal) is pyqtBoundSignal:
                    signal.connect(widget.__func)

            else:
                print("SaveWindow.RegisterWidget(" + str(wtype) + ") not implemented.")

        for widget in self.__widgets_size:  # type: QWidget
            size = s.value(widget.objectName() + '_size', None)
            if size is not None:
                widget.resize(size)

    def __widgets_savedata(self):
        s = self.Settings()
        for widget in self.__widgets_data:
            s.setValue(widget.objectName(), widget.Get_Value_Str())

        for widget in self.__widgets_size:
            s.setValue(widget.objectName() + '_size', widget.size())

    def show(self):
        if not self.__positioned:
            self.SettingsRestore()
            self.__widgets_setup()
            self.__positioned = True

    def closeEvent(self, e):
        self.SettingsSave()  # Window's data
        self.__widgets_savedata()  # Widget's data


class Window(QMainWindow, SaveWindow):
    def __init__(self, uifile_qwindow: str):
        QMainWindow.__init__(self)
        SaveWindow.__init__(self)
        loadUi(uifile_qwindow, self)

    def closeEvent(self, e):
        SaveWindow.closeEvent(self, e)

    def show(self):
        SaveWindow.show(self)
        QMainWindow.show(self)
        QMainWindow.setFocus(self)
        QMainWindow.activateWindow(self)


class Dialog(QDialog, SaveWindow):
    def __init__(self, uifile_qdialog: str):
        QDialog.__init__(self)
        SaveWindow.__init__(self)
        loadUi(uifile_qdialog, self)

    def closeEvent(self, e):
        SaveWindow.closeEvent(self, e)

    def show(self, modal: bool=False):
        SaveWindow.show(self)

        if modal:
            self.setWindowModality(Qt.ApplicationModal)
            QDialog.show(self)
            self.exec_()  # Modal
        else:
            QDialog.show(self)
            QDialog.setFocus(self)
            QDialog.activateWindow(self)
